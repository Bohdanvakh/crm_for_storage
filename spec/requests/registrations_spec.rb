require 'rails_helper'

RSpec.describe "Users", type: :request do
  describe "POST /users" do
    context "with valid parameters" do
      let(:user_params) do
        {
          email: "email@gmail.com",
          username: "username",
          role: "admin",
          password: "password",
          password_confirmation: "password"
        }
      end
      it "creates a new user" do
        expect do
          post "/users", params: {user: user_params}
        end.to change(User, :count).by(1)
      end
    end

    context "without email" do
      let(:user_params) do
        {
          email: "",
          username: "username",
          role: "admin",
          password: "password",
          password_confirmation: "password"
        }
      end
      it "does not create a new user" do
        expect do
          post "/users", params: {user: user_params}
        end.to_not change(User, :count)
      end
    end

    context "without username" do
      let(:user_params) do
        {
          email: "email@gmail.com",
          username: "",
          role: "admin",
          password: "password",
          password_confirmation: "password"
        }
      end
      it "does not create a new user" do
        expect do
          post "/users", params: {user: user_params}
        end.to_not change(User, :count)
      end
    end

    context "with invalid role" do
      let(:user_params) do
        {
          email: "email@gmail.com",
          username: "username",
          role: "invalid_role",
          password: "password",
          password_confirmation: "password"
        }
      end
      it "does not create a new user" do
        expect do
          post "/users", params: {user: user_params}
        end.to_not change(User, :count)
      end
    end
  end
end
