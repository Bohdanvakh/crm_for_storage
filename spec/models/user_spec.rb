require 'rails_helper'
user = User.new(email: "admin@gmail.com", username: "admin", role:"admin", password: "12345", password_confirmation: "12345")

describe User do
  describe "#create" do
    it "has all attributes" do
      post = {
        :email => user.email,
        :username => user.username,
        :role => user.role
      }
    end
  end
  describe "#registrations#new" do
    it "has invalid password" do
      invalid_user = User.new(email: "admin@gmail.com", username: "admin", role:"admin", password: "12345")

      expect(invalid_user).to_not be_valid
    end
    it "has empty email" do
      user = User.new(email: "", username: "admin", role:"admin", password: "12345678")

      expect(user).to_not be_valid
    end
    it "has empty username" do
      user = User.new(email: "admin@gmail.com", username: "", role:"admin", password: "12345678")
      expect(user).to_not be_valid
    end
    it "has empty role" do
      user = User.new(email: "admin@gmail.com", username: "admin", role:"", password: "12345678")
      expect(user).to_not be_valid
    end
    it "has invalid role" do
      user = User.new(email: "admin@gmail.com", username: "admin", role: "invalid_role", password: "12345678")
      expect(user).to_not be_valid
    end
    it "has valid attributes" do
      valid_user = User.new(email: "admin@gmail.com", username: "admin", role:"admin", password: "12345678")

      expect(valid_user).to be_valid
    end
  end
end
RSpec.describe User, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
