class AddRequiresToUser < ActiveRecord::Migration[7.0]
  def up
    change_column :users, :username, :string, null: false
    change_column :users, :role, :string, null: false
  end

  def down
    change_column :users, :username, :string
    change_column :users, :role, :string
  end
end
