class AddAttributesToUser < ActiveRecord::Migration[7.0]
  def up
    add_column :users, :username, :string
    add_column :users, :role, :string
  end
  def down
    remove_column :users, :username, :string
    remove_column :users, :role, :string
  end
end
