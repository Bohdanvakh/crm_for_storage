Rails.application.routes.draw do
  devise_for :users

  get "/", to: "home#index"
  get "/profile/:id", to: "profile#index", as: 'profile'
  #get '/profile/:id/edit/', to: "profile#edit", as: "profile_edit"
end
